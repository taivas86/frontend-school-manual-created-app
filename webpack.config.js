const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	entry: "./src/index.js",
	mode: "production",
	output: {
		filename: "main.[hash].js"
	},
	module: {
		rules: [
		{
			test: /\.s[ac]ss$/i,
			use: [
				MiniCssExtractPlugin.loader,
				"css-loader",
				"sass-loader"
			]
		}]
	},
	plugins: [
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'index.html'
		}),
		new MiniCssExtractPlugin({
			filename: "main.[hash].css"
		})
	],
	devServer: {
		contentBase: "./dist",
		port: 9000,
		watchContentBase: true,
		hot: true,
		open: true
	}
}