export class App {
	constructor(appElementId) {
		this.appElementId = appElementId;
		this.init();
	}

	init() {
		this.setInnerHTML(
			this.appElementId,
			`<div class='greetings'>
				Application says:
				<span id='appMessage' class='message'></span>
			</div>`
		);
	}
	
	start() {
		this.showGreetings();
	}
	
	showGreetings() {
		this.setInnerHTML('appMessage', 'Hello, world!');
	}
	
	setInnerHTML(elementId, html) {
		document.getElementById(elementId).innerHTML = html;
	}
}